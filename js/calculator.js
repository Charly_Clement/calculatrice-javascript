// tableau contenant toutes les opérations à effectuer
var operations = [];

// on rend tous les boutons cliquable
$('.btn').each(function () {
    $(this).click(function () {
        // on détermine quel bouton a été utilisé
        switch ($(this).html()) {
            case "0":
            case "1":
            case "2":
            case "3":
            case "4":
            case "5":
            case "6":
            case "7":
            case "8":
            case "9":
                // cas ou le bouton est un nombre :
                // on ajoute le nombre dans le tableau d'opérations
                addNumber($(this).html());
                break;

            case "*":
            case "/":
            case "+":
            case "-":
                // cas ou le bouton est un opérateur :
                // on ajoute l'opérateur dans le tableau d'opérations
                addOperator($(this).html());
                break;

            case "c":
                // on remet à 0 la calculatrice
                clear();
                break;

            case "=":
                // on effectue le calcul
                calculates();
                break;
        }
    })
});

// ajout d'un nombre dans le tableau d'opérations
function addNumber(number) {
    // on vérifie si il y a déjà un élément dans le tableau et si le dernier élement est un nombre
    if (operations.length > 0 && parseInt(operations[operations.length - 1]) == operations[operations.length - 1]) {
        // si le dernier élement est un nombre on concataine le nombre à la suite du dernier
        operations[operations.length - 1] += number;
    } else {
        // sinon on ajoute le nombre comme un nouvel element du tableau
        operations.push(number);
    }

    // affichage !
    display();
}

function isOperator(operator) {
    return operator === "*" || operator === "/" || operator === "-" || operator === "+";
}

// ajout d'un opérateur dans le tableau d'opérations
function addOperator(operator) {
    // on vérifie que le tableau n'est pas vide et on empêche l'ajout d'un opérateur en première position
    if (operations.length > 0) {
        // si le dernier element du tableau est un opérateur on le remplace
        if (isOperator(operations[operations.length - 1])) {
            operations[operations.length - 1] = operator;
        } else {
            // sinon on en ajoute un à la fin
            operations.push(operator);
        }
    }

    // affichage
    display();
}

function display() {
    // debug
    console.log(operations);

    // on vide la case "result"
    $('#result').html('');
    for (i = 0; i < operations.length; i++) {
        // on affiche tous les elements du tableau operations dans la page html
        $('#result').append(operations[i] + " ");
    }
}

// remise à 0 de la calculatrice
function clear() {
    operations = [];
    display();
}

// on effectue le calcul
function calculates() {

    // si le dernier element est un opérateur on le retire
    if (isOperator(operations[operations.length - 1])) {
        operations.splice(operations.length - 1);
    }

    // on effectue en premier les multiplications
    // en remplaçant les opérations du tableau par groupes de 3 par leur résultat
    // ex: ["2", "+", "3", "*", "6"]
    // devient : ["2", "+", 18]
    while (operations.indexOf("*") > 0) {
        operations.splice(operations.indexOf("*") - 1, 3, operations[operations.indexOf("*") - 1] * operations[operations.indexOf("*") + 1]);
    }

    // on effectue ensuite les divisions
    while (operations.indexOf("/") > 0) {
        operations.splice(operations.indexOf("/") - 1, 3, operations[operations.indexOf("/") - 1] / operations[operations.indexOf("/") + 1]);
    }

    // puis les soustractions
    while (operations.indexOf("-") > 0) {
        operations.splice(operations.indexOf("-") - 1, 3, operations[operations.indexOf("-") - 1] - operations[operations.indexOf("-") + 1]);
    }

    // puis les additions
    // ex : ["2", "+", 18]
    // devient : [20]
    while (operations.indexOf("+") > 0) {
        operations.splice(operations.indexOf("+") - 1, 3, parseInt(operations[operations.indexOf("+") - 1]) + parseInt(operations[operations.indexOf("+") + 1]));
    }

    // affichage
    display();
}

// lier les touches du clavier aux fonctions ci-dessus ...
$('body').keypress(function (event) {
    console.log(event.key);
    switch (event.key) {
        case "0":
        case "1":
        case "2":
        case "3":
        case "4":
        case "5":
        case "6":
        case "7":
        case "8":
        case "9":
            // cas ou le bouton est un nombre :
            // on ajoute le nombre dans le tableau d'opérations
            addNumber(event.key);
            break;

        case "*":
        case "/":
        case "+":
        case "-":
            // cas ou le bouton est un opérateur :
            // on ajoute l'opérateur dans le tableau d'opérations
            addOperator(event.key);
            break;

        case "c":
            // on remet à 0 la calculatrice
            clear();
            break;

        case "Enter":
        case "=":
            // on effectue le calcul
            calculates();
            break;
    }
})
